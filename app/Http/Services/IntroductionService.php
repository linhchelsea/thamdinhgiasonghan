<?php

namespace App\Http\Services;

use App\Http\Repositories\IntroductionRepository;
use Illuminate\Support\Facades\File;

class IntroductionService {
    function __construct() {
        $this->introRepo = new IntroductionRepository();
    }

    /**
     * get introduction
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getIntroduction () {
        return $this->introRepo->getIntroduction();
    }

    /**
     * update info
     * @param $params
     * @return bool
     */
    public function updateInfo ($params) {
        return $this->introRepo->updateInfo($params);
    }

    /**
     * update intro
     * @param $params
     * @return bool
     */
    public function updateIntro ($params) {
        return $this->introRepo->updateIntro($params);
    }
}