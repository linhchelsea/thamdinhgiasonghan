<?php

namespace App\Http\Services;

use App\Http\Repositories\UserRepository;

class UserService {
    function __construct() {
        $this->userRepo = new UserRepository();
    }

    /**
     * get all users
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getUsers () {
        return $this->userRepo->getUsers();
    }

    /**
     * get user by username
     * @param $username
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getUserByUsername ($username) {
        return $this->userRepo->getUserByUsername($username);
    }

    /**
     * get user by id
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getUserById ($id) {
        return $this->userRepo->getUserById($id);
    }

    /**
     * create new user
     * @param $params
     * @return bool
     */
    public function createNewUser ($params) {
        return $this->userRepo->createNewUser($params);
    }

    /**
     * post update user
     * @param $id
     * @param $params
     * @return bool
     */
    public function  updateUser ($id, $params) {
        return $this->userRepo->updateUser($id, $params);
    }

    /**
     * freeze user
     * @param $id
     * @return bool
     */
    public function freezeUser ($id) {
        return $this->userRepo->freezeUser($id);
    }
}