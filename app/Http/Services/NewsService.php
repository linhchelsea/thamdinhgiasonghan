<?php

namespace App\Http\Services;

use App\Http\Repositories\NewsRepository;
use Illuminate\Support\Facades\File;

class NewsService {
    function __construct() {
        $this->newsRepo = new NewsRepository();
    }

    /**
     * get all news
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getNews () {
        return $this->newsRepo->getNews();
    }

    /**
     * create news
     * @param $userId
     * @param $request
     * @return bool
     */
    public function createNews ($userId, $request) {
        $params = $request->all();
        $thumbnail = '';
        if($request->file('thumbnail') != null){
            $image = $request->thumbnail;
            $image = $request->file('thumbnail')->store('public/news');
            $arr_filename = explode("/",$image);
            $thumbnail = end($arr_filename);
        }
        $title = $params['title'];
        $content = $params['content'];
        return $this->newsRepo->createNews($userId, $title, $content, $thumbnail);
    }

    /**
     * get news by id
     * @param $id
     * @return mixed
     */
    public function getNewsById ($id) {
        return $this->newsRepo->getNewsById($id);
    }

    /**
     * @param $news
     * @param $request
     * @return bool
     */
    public function updateNews ($news, $request) {
        $params = $request->all();
        $thumbnail = $news['thumbnail'];
        if($request->file('thumbnail') != null){
            // xoa anh cu
            File::delete('storage/news/'.$news['thumbnail']);
            // upload anh moi
            $image = $request->thumbnail;
            $image = $request->file('thumbnail')->store('public/news');
            $arr_filename = explode("/",$image);
            $thumbnail = end($arr_filename);
        }
        $title = $params['title'];
        $content = $params['content'];
        return $this->newsRepo->updateNews($news['id'], $title, $content, $thumbnail);
    }

    /**
     * delete news
     * @param $news
     * @return bool
     */
    public function deleteNews ($news) {
        // xoa anh cua bai viet
        File::delete('storage/news/'.$news['thumbnail']);
        return $this->newsRepo->deleteNews($news['id']);
    }
}