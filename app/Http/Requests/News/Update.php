<?php

namespace App\Http\Requests\News;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255|min:6',
            'content' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Bạn chưa điền tiêu đề',
            'title.max'  => 'Tiêu đề quá dài',
            'title.min'  => 'Tiêu đề quá ngắn',
            'content.required' => 'Bạn chưa điền nội dung bài viết'
        ];
    }
}
