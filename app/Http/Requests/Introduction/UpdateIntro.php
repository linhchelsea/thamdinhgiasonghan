<?php

namespace App\Http\Requests\Introduction;

use Illuminate\Foundation\Http\FormRequest;

class UpdateIntro extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'introduction_title' => 'required',
            'introduction_content' => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'introduction_title.required' => 'Bạn chưa điền tiêu đề bài viết',
            'introduction_content.required' => 'Bạn chưa điền nội dung bài viết',
        ];
    }
}
