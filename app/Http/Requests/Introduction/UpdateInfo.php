<?php

namespace App\Http\Requests\Introduction;

use Illuminate\Foundation\Http\FormRequest;

class UpdateInfo extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'address' => 'required',
            'phone_code' => 'required',
            'phone_number' => 'required',
            'map' => 'required|url',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Bạn chưa điền email',
            'email.email' => 'Email không đúng định dạng',
            'address.required' => 'Bạn chưa điền địa chỉ công ty',
            'phone_code.required' => 'Bạn chưa nhập mã điện thoại',
            'phone_number.required' => 'Bạn chưa nhập số điện thoại',
            'map.required' => 'Bạn chưa nhập đường link địa chỉ Google Map',
            'map.url' => 'Đường dẫn không hợp lệ',
        ];
    }
}
