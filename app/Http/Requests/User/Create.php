<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:32|min:6',
            'email' => 'required|email',
            'firstName' => 'required',
            'lastName' => 'required',
            'position' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => 'Bạn chưa điền tên đăng nhập',
            'username.max'  => 'Tên đăng nhập không quá 32 kí tự',
            'username.min'  => 'Tên đăng nhập không dưới 6 kí tự',
            'email.required' => 'Bạn chưa điền email',
            'email.email' => 'Email không đúng định dạng',
            'firstName.required' => 'Bạn chưa điền tên và tên đệm',
            'lastName.required' => 'Bạn chưa điền họ',
            'position.required' => 'Bạn chưa chọn vị trí',
        ];
    }
}
