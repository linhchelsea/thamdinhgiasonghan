<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'firstName' => 'required',
            'lastName' => 'required',
            'position' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Bạn chưa điền email',
            'email.email' => 'Email không đúng định dạng',
            'firstName.required' => 'Bạn chưa điền tên và tên đệm',
            'lastName.required' => 'Bạn chưa điền họ',
            'position.required' => 'Bạn chưa chọn vị trí',
        ];
    }
}
