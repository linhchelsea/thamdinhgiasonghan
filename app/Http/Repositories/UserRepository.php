<?php

namespace App\Http\Repositories;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserRepository {
    function __construct() {
        $this->pagination = 10;
    }

    /**
     * get users
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getUsers () {
        return User::query()
            ->where('position','<>',  1)
            ->paginate(10);
    }

    /**
     * create new user
     * @param $params
     * @return bool
     */
    public function createNewUser ($params) {
        try {
            $user = new User();
            $user->username = $params['username'];
            $user->password = Hash::make('123123');;
            $user->email = $params['email'];
            $user->first_name = $params['firstName'];
            $user->last_name = $params['lastName'];
            $user->position = $params['position'];
            $user->remember_token = '';
            $user->save();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * post update user
     * @param $id
     * @param $params
     * @return bool
     */
    public function updateUser ($id, $params) {
        try {
            $user = User::findOrFail($id);
            $user->email = $params['email'];
            $user->first_name = $params['firstName'];
            $user->last_name = $params['lastName'];
            $user->position = $params['position'];

            if ($params['password']) {
                $user->password = Hash::make($params['password']);;
            }

            $user->save();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * freeze user
     * @param $id
     * @return bool
     */
    public function freezeUser ($id) {
        try {
            $user = User::findOrFail($id);
            $user->is_freeze = true;
            $user->save();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * get user by username
     * @param $username
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function getUserByUsername ($username) {
        return User::query()
            ->where('username', $username)
            ->first();
    }

    /**
     * get user by id
     * @param $id
     * @return mixed
     */
    public function getUserById ($id) {
        return User::find($id);
    }
}