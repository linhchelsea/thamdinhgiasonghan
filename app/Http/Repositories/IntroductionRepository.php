<?php

namespace App\Http\Repositories;
use App\Introduction;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rules\In;

class IntroductionRepository {
    /**
     * get introduction
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getIntroduction () {
        return Introduction::find(1);
    }

    /**
     * update info
     * @param $params
     * @return bool
     */
    public function updateInfo ($params) {
        try {
            $introduction = Introduction::find(1);
            if (!$introduction) {
                $introduction = new Introduction();
                $introduction->introduction_title = '';
                $introduction->introduction_content = '';
            }
            $introduction->email = $params['email'];
            $introduction->address = $params['address'];
            $introduction->phone_code = $params['phone_code'];
            $introduction->phone_number = $params['phone_number'];
            $introduction->fax_number = $params['fax_number'];
            $introduction->map = $params['map'];
            $introduction->save();
            return true;
        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }
    }

    /**
     * update introduction
     * @param $params
     * @return bool
     */
    public function updateIntro ($params) {
        try {
            $introduction = Introduction::find(1);
            if (!$introduction) {
                $introduction = new Introduction();
                $introduction->email = '';
                $introduction->address = '';
                $introduction->phone_code = '';
                $introduction->phone_number = '';
                $introduction->fax_number = '';
                $introduction->map = '';
            }
            $introduction->introduction_title = $params['introduction_title'];
            $introduction->introduction_content = $params['introduction_content'];
            $introduction->save();
            return true;
        } catch (\Exception $e) {
            Log::error($e);
            return false;
        }
    }
}