<?php

namespace App\Http\Repositories;
use App\News;

class NewsRepository {
    function __construct() {
        $this->pagination = 10;
    }

    /**
     * get news
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getNews () {
        return News::query()
            ->with('user')
            ->orderBy('id', 'desc')
            ->paginate($this->pagination);
    }

    /**
     * create news
     * @param $userId
     * @param $title
     * @param $content
     * @param $thumbnail
     * @return bool
     */
    public function createNews ($userId, $title, $content, $thumbnail) {
        try {
            $news = new News();
            $news->user_id = $userId;
            $news->title = $title;
            $news->content = $content;
            $news->thumbnail = $thumbnail;
            $news->save();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * get news by id
     * @param $id
     * @return mixed
     */
    public function getNewsById ($id) {
        return News::find($id);
    }

    /**
     * update news
     * @param $id
     * @param $title
     * @param $content
     * @param $thumbnail
     * @return bool
     */
    public function updateNews ($id, $title, $content, $thumbnail) {
        try {
            $news = News::findOrFail($id);
            $news->title = $title;
            $news->content = $content;
            $news->thumbnail = $thumbnail;
            $news->save();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * delete news
     * @param $id
     * @return bool
     */
    public function deleteNews ($id) {
        try {
            $news = News::findOrFail($id);
            $news->delete();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}