<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Controllers\Controller;
use App\Http\Requests\Introduction\UpdateInfo;
use App\Http\Requests\Introduction\UpdateIntro;
use App\Http\Services\IntroductionService;

class IntroductionController extends Controller
{
    function __construct() {
        $this->introService = new IntroductionService();
    }

    /**
     * get introduction
     * @return $this
     */
    public function getIndex () {
        $introduction = $this->introService->getIntroduction();
        return view('backend.introduction.index', compact('introduction'));
    }

    /**
     * update company information
     * @param UpdateInfo $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdateInfo (UpdateInfo $request) {
        $request->validated();
        $params = $request->all();
        $isUpdate = $this->introService->updateInfo($params);
        if ($isUpdate) {
            $request->session()->flash('success','Cập nhật thành công');
        } else{
            $request->session()->flash('fail','Cập nhật không thành công');
        }
        return redirect()->route('introduction.index');
    }

    /**
     * update company introduction
     * @param UpdateIntro $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdateIntro (UpdateIntro $request) {
        $request->validated();
        $params = $request->all();
        $isUpdate = $this->introService->updateIntro($params);
        if ($isUpdate) {
            $request->session()->flash('success','Cập nhật thành công');
        } else{
            $request->session()->flash('fail','Cập nhật không thành công');
        }
        return redirect()->route('introduction.index');
    }
}
