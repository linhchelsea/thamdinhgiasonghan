<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests\User\Create;
use App\Http\Requests\User\Update;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\UserService;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    function __construct() {
        $this->userService = new UserService();
    }

    /**
     * get user index
     * @return $this
     */
    public function getIndex () {
        $users = $this->userService->getUsers();
        return view('backend.users.index')
            ->with('users', $users);
    }

    /**
     * get create view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate () {
        return view('backend.users.create');
    }

    /**
     * store new user
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postStore (Create $request) {
        $request->validated();

        $params = $request->all();
        $user = $this->userService->getUserByUsername($params['username']);
        if ($user) {
            $request->session()->flash('fail', 'Tên đăng nhập đã được sử dụng');
            return redirect()->back();
        }
        $isSuccess = $this->userService->createNewUser($params);
        if ($isSuccess) {
            $request->session()->flash('success', 'Thêm mới thành công');
            return redirect()->route('users.index');
        }
        $request->session()->flash('fail', 'Thêm mới không thành công');
        return redirect()->back();
    }

    /**
     * get edit user
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getEdit (Request $request, $id) {
        $user = $this->userService->getUserById($id);
        if (!$user) {
            $request->session()->flash('fail', 'Người dùng không tồn tại');
            return redirect()->route('users.index');
        }
        if ($user['position'] === 1) {
            $request->session()->flash('fail', 'Truy cập bị từ chối');
            return redirect()->route('users.index');
        }
        return view('backend.users.edit', compact('user'));
    }

    /**
     * post update user
     * @param Update $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate (Update $request, $id) {
        $request->validated();
        $params = $request->all();
        if ($params['password']) {
            if (strlen($params['password']) > 32) {
                $request->session()->flash('fail', 'Mật khẩu không quá 32 kí tự');
                return redirect()->route('users.edit', $id);
            }
            if (strlen($params['password']) < 6) {
                $request->session()->flash('fail', 'Mật khẩu không dưới 6 kí tự');
                return redirect()->route('users.edit', $id);
            }
        }
        $user = $this->userService->getUserById($id);
        if (!$user) {
            $request->session()->flash('fail', 'Người dùng không tồn tại');
            return redirect()->route('users.index');
        }
        if ($user['position'] === 1) {
            $request->session()->flash('fail', 'Truy cập bị từ chối');
            return redirect()->route('users.index');
        }

        $isSuccess = $this->userService->updateUser($id, $params);
        if ($isSuccess) {
            $request->session()->flash('success', 'Cập nhật thành công');
        } else {
            $request->session()->flash('fail', 'Cập nhật không thành công');
        }
        return redirect()->route('users.edit', $id);
    }

    public function getFreezeUser (Request $request, $id) {
        $user = $this->userService->getUserById($id);
        if (!$user) {
            $request->session()->flash('fail', 'Người dùng không tồn tại');
            return redirect()->route('users.index');
        }
        if ($user['position'] === 1) {
            $request->session()->flash('fail', 'Truy cập bị từ chối');
            return redirect()->route('users.index');
        }
        $isSuccess = $this->userService->freezeUser($id);
        if ($isSuccess) {
            $request->session()->flash('success', 'Khóa người dùng thành công');
        } else {
            $request->session()->flash('fail', 'Khóa người dùng không thành công');
        }
        return redirect()->back();
    }
}
