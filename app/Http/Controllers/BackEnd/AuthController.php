<?php

namespace App\Http\Controllers\BackEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller {
    /**
     * render login view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLogin () {
        return view('backend.pages.login');
    }

    /**
     * post login
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postLogin (Request $request) {
        $params = $request->only(['username', 'password']);
        if (Auth::attempt($params)) {
            return redirect()->route('admin.index');
        }
        return redirect()->back();
    }

    /**
     * logout
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout (Request $request) {
        Auth::logout();
        return redirect()->route('login');
    }
}
