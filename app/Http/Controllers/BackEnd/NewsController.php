<?php

namespace App\Http\Controllers\BackEnd;

use App\Http\Requests\News\Create;
use App\Http\Controllers\Controller;
use App\Http\Requests\News\Update;
use App\Http\Services\NewsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    function __construct() {
        $this->newsService = new NewsService();
    }

    /**
     * get user index
     * @return $this
     */
    public function getIndex () {
        $news = $this->newsService->getNews();
        return view('backend.news.index')
            ->with('news', $news);
    }

    /**
     * get create view
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate () {
        return view('backend.news.create');
    }

    /**
     * create news
     * @param Create $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postStore (Create $request) {
        $request->validated();
        $userId = Auth::id();
        $isStore = $this->newsService->createNews($userId, $request);
        if ($isStore) {
            $request->session()->flash('success','Thêm thành công');
            return redirect()->route('news.index');
        }
        $request->session()->flash('fail','Thêm không thành công');
        return redirect()->route('news.create');
    }

    /**
     * get edit view
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getEdit (Request $request, $id) {
        $news = $this->newsService->getNewsById($id);
        if (!$news) {
            $request->session()->flash('fail', 'Không tìm thấy bài viết');
            return redirect()->back();
        }
        $user = Auth::user();
        if ($user->id !== $news['user_id'] && $user->position !== 1) {
            $request->session()->flash('fail', 'Truy cập bị từ chối');
            return redirect()->back();
        }
        return view('backend.news.edit', compact('news'));
    }

    /**
     * update news
     * @param Update $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUpdate (Update $request, $id) {
        $request->validated();
        $news = $this->newsService->getNewsById($id);
        if (!$news) {
            $request->session()->flash('fail', 'Không tìm thấy bài viết');
            return redirect()->back();
        }
        $user = Auth::user();
        if ($user->id !== $news['user_id'] && $user->position !== 1) {
            $request->session()->flash('fail', 'Truy cập bị từ chối');
            return redirect()->back();
        }

        // update news
        $isUpdate = $this->newsService->updateNews($news, $request);
        if ($isUpdate) {
            $request->session()->flash('success','Cập nhật thành công');
        } else {
            $request->session()->flash('fail','Cập nhật không thành công');
        }
        return redirect()->route('news.edit', $id);
    }

    /**
     * getDelete
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete (Request $request, $id) {
        $news = $this->newsService->getNewsById($id);
        if (!$news) {
            $request->session()->flash('fail', 'Không tìm thấy bài viết');
            return redirect()->back();
        }
        $user = Auth::user();
        if ($user->id !== $news['user_id'] && $user->position !== 1) {
            $request->session()->flash('fail', 'Truy cập bị từ chối');
            return redirect()->back();
        }

        // update news
        $isDelete = $this->newsService->deleteNews($news);
        if ($isDelete) {
            $request->session()->flash('success','Xoá bài viết thành công');
        } else {
            $request->session()->flash('fail','Xoá bài viết không thành công');
        }
        return redirect()->route('news.index');
    }
}
