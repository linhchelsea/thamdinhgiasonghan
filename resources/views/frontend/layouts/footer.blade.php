<div class="sha-footer">
    <div class="container d-flex flex-row justify-content-center justify-content-sm-start align-items-start flex-wrap flex-sm-nowrap py-3">
        <div class="d-none d-md-block logo align-self-center px-3">
            <img src="{{URL::asset('frontend/assets/images/logo.png')}}" height="90" width="auto" alt="logo"
                 class="logo">
        </div>
        <div class="content row w-100">
            <div class="col-12 col-md-9">
                <h3 class="title mb-3">CÔNG TY CỔ PHẦN THẨM ĐỊNH GIÁ SÔNG HÀN</h3>
                <div class="info-row">
                    <span class="info-icon"><i class="fas fa-map-marker-alt"></i></span>
                    <span class="info-text">79 Hồ Tùng Mậu - Q. Liên Chiểu - TP. Đà Nẵng</span>
                </div>
                <div class="info-row my-2">
                    <span class="info-icon"><i class="far fa-envelope"></i></span>
                    <span class="info-text">thamdinhgiasonghan@gmail.com</span>
                </div>
                <div class="info-row">
                    <span class="info-icon"><i class="fas fa-phone-volume"></i></span>
                    <span class="info-text">0914 323 521</span>
                </div>
            </div>
            <div class="col-12 col-md-3 mt-4 mt-md-0">
                <h3 class="title mb-1">LIÊN KẾT</h3>
                <div class="title-underline mb-2"></div>
                <div>
                    <span><a class="icon-circle" href="#"><i class="fab fa-google-plus-g"></i></a></span>
                    <span class="ml-2"><a class="icon-circle" href="#"><i class="fab fa-facebook-f"></i></a></span>
                </div>
            </div>
        </div>
    </div>
</div>
