<div class="sha-banner">
    <div class="container d-flex flex-row justify-content-center justify-content-sm-start align-items-center flex-wrap flex-sm-nowrap py-2">
        <div class="mb-3 mb-sm-0">
            <img src="{{URL::asset('frontend/assets/images/logo-red.png')}}" height="120" width="auto" alt="logo"
                 class="logo">
        </div>
        <div class="sha-banner-text d-flex justify-content-center align-items-center flex-column w-100">
            <span class="text-center" style="font-weight: 900; line-height: 20px;">CÔNG TY CỔ PHẦN THẨM ĐỊNH GIÁ SÔNG HÀN</span>
            <span class="text-center" style="font-size: 85%; font-weight: 700">HAN RIVER VALUATION JOINT STOCK COMPANY</span>
            <div class="mb-2 d-none d-sm-block" style="height: 1px; background-color: #D22B27; width: 200px;"></div>
            <span class="text-center d-none d-sm-block" style="font-size: 85%; font-weight: 700">Địa chỉ: 79 Hồ Tùng Mậu - Q. Liên Chiểu - TP. Đà Nẵng</span>
            <span class="text-center d-none d-sm-block" style="font-size: 85%; font-weight: 700; line-height: 20px;">Điện thoại: 0914 323 521 - Email: thamdinhgiasonghan@gmail.com</span>
        </div>
    </div>
</div>
<nav class="navbar sticky-top navbar-light navbar-expand-lg sha-main-menubar">
    <div class="container">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler my-2 mr-3 mr-sm-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span style="color: #ffeeaa;"><i class="fas fa-bars fa-lg"></i></span>
        </button>

        <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item active mx-0 mx-lg-2">
                    <a class="nav-link" href="#">TRANG CHỦ<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">GIỚI THIỆU</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        DỊCH VỤ
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="bottom-border d-none d-lg-block"></div>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">TIN TỨC</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">VĂN BẢN</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">LIÊN HỆ</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
