<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

    <title>Tham Dinh Gia Song Han - SHA</title>

    <!-- Loading main css file -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600|Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('frontend/css/app.css') }}">
</head>
<body>
@include('frontend.layouts.menu-bar')

<!-- content -->
@yield('content')

@include('frontend.layouts.footer')

<!-- Main JS file -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script type="text/javacript" src="{{ asset('frontend/js/app.js') }}"></script>
</body>
</html>
