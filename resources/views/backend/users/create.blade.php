@extends('backend/layout')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">supervisor_account</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">THÊM NGƯỜI DÙNG</h4>
                            <form class="form-horizontal" method="POST" action="{{ route('users.store') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <label class="col-md-3 label-on-left">EMAIL</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="email" class="form-control" name="email" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">TÊN ĐĂNG NHẬP</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text" class="form-control" name="username" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">HỌ</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text" class="form-control" name="lastName" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">TÊN ĐỆM VÀ TÊN</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text" class="form-control" name="firstName" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">VỊ TRÍ</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <select class="form-control"
                                                    data-style="select-with-transition"
                                                    name="position"
                                                    required="required"
                                            >
                                                <option value="2">Giám đốc</option>
                                                <option value="3">Quản lý</option>
                                                <option value="4">Nhân viên</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3"></label>
                                    <div class="col-md-9">
                                        <div class="form-group form-button">
                                            <button type="submit" class="btn btn-fill btn-primary">THÊM</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection