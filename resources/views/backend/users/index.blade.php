@extends('backend/layout')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">supervisor_account</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">NGƯỜI DÙNG</h4>
                            <div class="table-responsive">
                                <a href="{{ route('users.create') }}" class="btn btn-facebook">Thêm người dùng</a>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center header-gray">#</th>
                                        <th class="text-center header-gray">Tên đăng nhập</th>
                                        <th class="text-center header-gray">Họ và tên</th>
                                        <th class="text-center header-gray">Vị trí</th>
                                        <th class="text-center header-gray">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($users) == 0)
                                    <tr>
                                        <td colspan="6" class="text-center text-blue">
                                            <h4>NO USER TO SHOW</h4>
                                        </td>
                                    </tr>
                                    @else
                                        @foreach($users as $key => $user)
                                    <tr>
                                        <td class="text-center">{{ $key + 1 }}</td>
                                        <td class="text-center">{{ $user->username }}</td>
                                        <td class="text-center">{{ $user->first_name }} {{ $user->last_name }}</td>
                                        <td class="text-center">{{ $user->position }}</td>
                                        <td class="td-actions text-center">
                                            <a href="{{ route('users.edit', $user->id) }}" rel="tooltip" class="btn btn-success">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <a href="{{ route('users.freeze', $user->id) }}" rel="tooltip" class="btn btn-danger">
                                                <i class="material-icons">close</i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="box-footer clearfix">
                                <div class="pagination-sm no-margin pull-right">
                                    {{ $users->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection