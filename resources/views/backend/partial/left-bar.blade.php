<div class="sidebar" data-active-color="rose" data-background-color="black" data-image="/backend/img/sidebar-1.jpg">
  <div class="logo">
    <a href="http://www.creative-tim.com" class="simple-text logo-mini">
      CT
    </a>
    <a href="http://www.creative-tim.com" class="simple-text logo-normal">
      Creative Tim
    </a>
  </div>
  <div class="sidebar-wrapper">
    <div class="user">
      <div class="photo">
        <img src="/backend/img/faces/avatar.jpg" />
      </div>
      <div class="info">
        <a data-toggle="collapse" href="#collapseExample" class="collapsed">
          <span>
            {{ Auth::user()->last_name }} {{ Auth::user()->first_name }}
            <b class="caret"></b>
          </span>
        </a>
        <div class="clearfix"></div>
        <div class="collapse" id="collapseExample">
          <ul class="nav">
            <li>
              <a href="#">
                <span class="sidebar-mini"> EP </span>
                <span class="sidebar-normal"> Chỉnh sửa </span>
              </a>
            </li>
            <li>
              <a href="{{ route('getLogout') }}">
                <span class="sidebar-mini"> LO </span>
                <span class="sidebar-normal"> Đăng xuất </span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <ul class="nav">
      <li class="active">
        <a href="">
          <i class="material-icons">dashboard</i>
          <p> TRANG CHÍNH </p>
        </a>
      </li>
      <li>
        <a href="{{ route('users.index') }}">
          <i class="material-icons">supervisor_account</i>
          <p> NGƯỜI DÙNG </p>
        </a>
      </li>
      <li>
        <a href="{{ route('news.index') }}">
          <i class="material-icons">picture_in_picture</i>
          <p> BÀI VIẾT </p>
        </a>
      </li>
      <li>
        <a href="{{ route('introduction.index') }}">
          <i class="material-icons">airplay</i>
          <p> GIỚI THIỆU </p>
        </a>
      </li>
    </ul>
  </div>
</div>
