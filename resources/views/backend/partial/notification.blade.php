@if(Session::has('success'))
    <script>
        demo.showNotification('top','right', 'success', '{{ Session::get('success') }}');
    </script>
@endif
@if(Session::has('fail'))
    <script>
        demo.showNotification('top','right', 'danger', '{{ Session::get('fail') }}');
    </script>
@endif

@if($errors->count()>0)
    <script>
    @foreach($errors->all() as $error)
        demo.showNotification('top','right', 'danger', '{{ $error }}');
    @endforeach
    </script>
@endif