@extends('backend/layout')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">picture_in_picture</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">BÀI VIẾT</h4>
                            <div class="table-responsive">
                                <a href="{{ route('news.create') }}" class="btn btn-facebook">Tạo bài viết</a>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="text-center header-gray" width="5%">#</th>
                                        <th class="text-center header-gray" width="20%">Ảnh</th>
                                        <th class="text-center header-gray" width="30%" style="word-break: break-all;">Tiêu đề</th>
                                        <th class="text-center header-gray" width="20%">Người đăng</th>
                                        <th class="text-center header-gray" width="15%">Thời gian tạo</th>
                                        <th class="text-center header-gray" width="10%">Tác vụ</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($news) == 0)
                                    <tr>
                                        <td colspan="6" class="text-center text-blue">
                                            <h4>CHƯA CÓ BÀI VIẾT NÀO</h4>
                                        </td>
                                    </tr>
                                    @else
                                        @foreach($news as $key => $item)
                                    <tr>
                                        <td class="text-center">{{ $key + 1 }}</td>
                                        <td class="text-center">
                                            <img class="img-thumbnail"
                                                 src="{{ $item->thumbnail ? asset('storage/news/'.$item->thumbnail) : asset('backend/img/default.png')}}"
                                                 style="width: 300px;height: 200px; object-fit: cover;"
                                            />
                                        </td>
                                        <td class="text-center">{{ $item->title }}</td>
                                        <td class="text-center">{{ $item->user->username }}</td>
                                        <td class="text-center">{{ $item->created_at }}</td>
                                        <td class="td-actions text-center">
                                            @if(Auth::user()->position === 1 || (Auth::user()->position !== 1 && Auth::user()->id === $item->user_id))
                                            <a href="{{ route('news.edit', $item->id) }}" rel="tooltip" class="btn btn-success">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <a href="{{ route('news.delete', $item->id) }}"
                                               rel="tooltip"
                                               class="btn btn-danger"
                                               onclick="return confirm('Bạn muốn xóa bài viết này?')"
                                            >
                                                <i class="material-icons">close</i>
                                            </a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="box-footer clearfix">
                                <div class="pagination-sm no-margin pull-right">
                                    {{ $news->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
@endsection