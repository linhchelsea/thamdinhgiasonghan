@extends('backend/layout')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">picture_in_picture</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">CHỈNH SỬA BÀI VIẾT MỚI</h4>
                            <form class="form-horizontal" method="POST" action="{{ route('news.update', $news->id) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <label class="col-md-3 label-on-left">CHỌN ẢNH THUMBNAIL</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input class="form-control"
                                                   name="thumbnail"
                                                   type="file"
                                                   id="image"
                                                   accept="image/x-png,image/gif,image/jpeg"
                                                   onchange="viewImage(this)"
                                            >
                                            <br>
                                            <p><img id="image-show"
                                                    src="{{ $news->thumbnail ? asset('storage/news/'.$news->thumbnail) : asset('backend/img/default.png')}}"
                                                    alt="avatar"
                                                    class="img-responsive"
                                                    style="width:300px;height:200px;object-fit: cover"
                                                >
                                            </p>
                                            <p>Click vào ảnh để tải ảnh lên</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">TIÊU ĐỀ</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="title"
                                                   value="{{ $news->title }}"
                                                   required
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">NỘI DUNG</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <textarea class="ckeditor" name="content" id="content">
                                                {{ $news->content }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3"></label>
                                    <div class="col-md-9">
                                        <div class="form-group form-button">
                                            <button type="submit" class="btn btn-fill btn-success">CẬP NHẬT</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('/backend/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/backend/js/image.js') }}"></script>
@endsection
@section('js')
@endsection