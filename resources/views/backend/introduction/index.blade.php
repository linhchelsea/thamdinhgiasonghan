@extends('backend/layout')
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">airplay</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">GIỚI THIỆU VỀ CÔNG TY</h4>
                            <form class="form-horizontal"
                                  method="POST"
                                  action="{{ route('introduction.updateInfo') }}"
                            >
                                {{ csrf_field() }}
                                <div class="row">
                                    <label class="col-md-3 label-on-left">Địa chỉ email</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="email"
                                                   class="form-control"
                                                   name="email"
                                                   required
                                                   value="{{ $introduction ? $introduction->email : '' }}"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">Địa chỉ công ty</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="address"
                                                   required
                                                   value="{{ $introduction ? $introduction->address : '' }}"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">Số điện thoại</label>
                                    <div class="col-md-1">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="phone_code"
                                                   required
                                                   value="{{ $introduction ? $introduction->phone_code : '+84' }}"
                                            >
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="phone_number"
                                                   required
                                                   value="{{ $introduction ? $introduction->phone_number : '' }}"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">Số fax</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="fax_number"
                                                   required
                                                   value="{{ $introduction ? $introduction->fax_number : '' }}"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">Địa chỉ trên google map</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="map"
                                                   required
                                                   value="{{ $introduction ? $introduction->map : '' }}"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3"></label>
                                    <div class="col-md-9">
                                        <div class="form-group form-button">
                                            <button type="submit" class="btn btn-fill btn-primary">CẬP NHẬT</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">picture_in_picture</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">BÀI GIỚI THIỆU</h4>
                            <form class="form-horizontal"
                                  method="POST"
                                  action="{{ route('introduction.updateIntro') }}"
                            >
                                {{ csrf_field() }}
                                <div class="row">
                                    <label class="col-md-3 label-on-left">Tiêu đề</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <input type="text"
                                                   class="form-control"
                                                   name="introduction_title"
                                                   required
                                                   value="{{ $introduction ? $introduction->introduction_title : '' }}"
                                            >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 label-on-left">Nội dung</label>
                                    <div class="col-md-9">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label"></label>
                                            <textarea class="ckeditor"
                                                      name="introduction_content"
                                                      id="content"
                                            >{{ $introduction ? $introduction->introduction_content : '' }}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3"></label>
                                    <div class="col-md-9">
                                        <div class="form-group form-button">
                                            <button type="submit" class="btn btn-fill btn-primary">CẬP NHẬT</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('/backend/js/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/backend/js/image.js') }}"></script>
@endsection
@section('js')
@endsection