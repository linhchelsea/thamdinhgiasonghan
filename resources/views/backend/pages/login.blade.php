@extends('backend.pages.layout')
  @section('content')
<div class="full-page lock-page" filter-color="black">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
          <form method="post" action="{{ route('postLogin') }}">
            {{ csrf_field() }}
            <div class="card card-login card-hidden">
              <div class="card-header text-center" data-background-color="rose">
                <h4 class="card-title">ĐĂNG NHẬP</h4>
              </div>
              <div class="card-content">
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="material-icons">face</i>
                  </span>
                  <div class="form-group label-floating">
                    <label class="control-label">Tên đăng nhập</label>
                    <input type="text" class="form-control" name="username"/>
                  </div>
                </div>
                <div class="input-group">
                  <span class="input-group-addon">
                    <i class="material-icons">lock_outline</i>
                  </span>
                  <div class="form-group label-floating">
                    <label class="control-label">Mật khẩu</label>
                    <input type="password" class="form-control" name="password"/>
                  </div>
                </div>
              </div>
              <div class="footer text-center">
                <button type="submit" class="btn btn-rose btn-simple btn-wd btn-lg">Đăng nhập</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
  @endsection
  @section('js')
<script type="text/javascript">
$().ready(function () {
  demo.checkFullPageBackgroundImage();

  setTimeout(function () {
    // after 1000 ms we add the class animated to the login/register card
    $('.card').removeClass('card-hidden');
  }, 700)
});
</script>
  @endsection
