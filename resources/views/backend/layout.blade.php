<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8"/>
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png"/>
  <link rel="icon" type="image/png" href="../assets/img/favicon.png"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <title>Material Dashboard Pro by Creative Tim</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
        name='viewport'/>
  <meta name="viewport" content="width=device-width"/>
  <!-- Bootstrap core CSS     -->
  <!--  Material Dashboard CSS    -->
  <!--  CSS for Demo Purpose, don't include it in your project     -->
  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="{{ asset('/backend/css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('/backend/css/material-dashboard.css?v=1.2.1') }}" />
  <link rel="stylesheet" href="{{ asset('/backend/css/demo.css') }}"/>
  <link rel="stylesheet" href="{{ asset('http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css') }}"/>
  <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"/>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>
</head>

<body>
<div class="wrapper">
  @include('backend.partial.left-bar')
  <div class="main-panel">
    @include('backend.partial.header')
    @yield('content')
    @include('backend.partial.footer')
  </div>
</div>
</body>
<!--   Core JS Files   -->
<script src="{{ asset('/backend/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('/backend/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/backend/js/material.min.js') }}"></script>
<script src="{{ asset('/backend/js/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('/ttps://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js') }}"></script>
<script src="{{ asset('/backend/js/arrive.min.js') }}"></script>
<script src="{{ asset('/backend/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('/backend/js/moment.min.js') }}"></script>
<script src="{{ asset('/backend/js/chartist.min.js') }}"></script>
<script src="{{ asset('/backend/js/jquery.bootstrap-wizard.js') }}"></script>
<script src="{{ asset('/backend/js/bootstrap-notify.js') }}"></script>
<script src="{{ asset('/backend/js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('/backend/js/jquery-jvectormap.js') }}"></script>
<script src="{{ asset('/backend/js/nouislider.min.js') }}"></script>
<script src="{{ asset('/ttps://maps.googleapis.com/maps/api/js?key=AIzaSyB1xE6coSSZDCHBKp2o4gaO2-0dUFxn9vA') }}"></script>
<script src="{{ asset('/backend/js/jquery.select-bootstrap.js') }}"></script>
<script src="{{ asset('/backend/js/jquery.datatables.js') }}"></script>
<script src="{{ asset('/backend/js/sweetalert2.js') }}"></script>
<script src="{{ asset('/backend/js/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('/backend/js/fullcalendar.min.js') }}"></script>
<script src="{{ asset('/backend/js/jquery.tagsinput.js') }}"></script>
<script src="{{ asset('/backend/js/material-dashboard.js?v=1.2.1') }}"></script>
<script src="{{ asset('/backend/js/demo.js') }}"></script>
@section('js')
@endsection

@include('backend.partial.notification')
</html>
