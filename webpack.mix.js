const mix = require('laravel-mix');

mix.sass('resources/frontend/scss/app.scss', 'public/frontend/css/')
    .js('resources/frontend/js/app.js', 'public/frontend/js');
