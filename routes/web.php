<?php

use App\Http\Middleware\Authentication;
use App\Http\Middleware\CheckLogin;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend/home');
});

Route::group(['prefix' => 'admin', 'middleware' => [Authentication::class], 'namespace' => 'BackEnd'], function () {
    Route::get('/', 'HomeController@getHomePage')->name('admin.index');
    Route::get('/logout', 'AuthController@getLogout')->name('getLogout');

    /**
     * =======================================================================
     *                              USER ROUTE
     * =======================================================================
     */
    Route::get('/users', 'UserController@getIndex')->name('users.index')->middleware('checkSuperAdmin');
    Route::get('/users/create', 'UserController@getCreate')->name('users.create')->middleware('checkSuperAdmin');
    Route::post('/users/store', 'UserController@postStore')->name('users.store')->middleware('checkSuperAdmin');
    Route::get('/users/{id}/edit', 'UserController@getEdit')->name('users.edit')->middleware('checkSuperAdmin');
    Route::post('/users/{id}/update', 'UserController@postUpdate')->name('users.update')->middleware('checkSuperAdmin');
    Route::get('/users/{id}/freeze', 'UserController@getFreezeUser')->name('users.freeze')->middleware('checkSuperAdmin');

    /**
     * =======================================================================
     *                              NEWS ROUTE
     * =======================================================================
     */
    Route::get('/news', 'NewsController@getIndex')->name('news.index');
    Route::get('/news/create', 'NewsController@getCreate')->name('news.create');
    Route::post('/news/store', 'NewsController@postStore')->name('news.store');
    Route::get('/news/{id}/edit', 'NewsController@getEdit')->name('news.edit');
    Route::post('/news/{id}/update', 'NewsController@postUpdate')->name('news.update');
    Route::get('/news/{id}/delete', 'NewsController@getDelete')->name('news.delete');

    /**
     * =======================================================================
     *                              INTRODUCTION ROUTE
     * =======================================================================
     */
    Route::get('/introduction', 'IntroductionController@getIndex')->name('introduction.index');
    Route::post('/introduction/updateInfo', 'IntroductionController@postUpdateInfo')->name('introduction.updateInfo');
    Route::post('/introduction/updateIntro', 'IntroductionController@postUpdateIntro')->name('introduction.updateIntro');
});

Route::group(['prefix' => 'admin', 'middleware' => [CheckLogin::class], 'namespace' => 'BackEnd'], function () {
    Route::get('/login', 'AuthController@getLogin')->name('login');
    Route::post('/login', 'AuthController@postLogin')->name('postLogin');
});
